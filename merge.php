<?php
$branch = @$_GET['branch'];
if ($branch=="") {$branch="main";}

if (file_exists("main")&&($branch!="main")) {
	die("Only the main branch can be merged here");
}

define(ROOT,true);
define(SOURCE,false);

$newSourceIndex=trim(getGitlabSource(ROOT,$branch,"files.dat"));
if ($newSourceIndex=="") {die("Branch $branch not found");}

$newSourceFiles=explode("\n",$newSourceIndex);
if (!file_exists("files.dat")){ // First time
	file_put_contents("files.dat",$newSourceIndex);
	foreach ($newSourceFiles as $file) {
		$source=getGitlabSource(SOURCE,$branch,$file);
		if ($source=="") {die("Source $file not found");}
		$parts=explode("/",$file);
		$dir="";
		if (count($parts)>1) {
			for ($i=0;$i<count($parts)-1;$i++){
				if ($dir!="") {$dir.="/";}
				$dir.=$parts[$i];
				if (!file_exists($dir)) {mkdir($dir);}
			}
		}
		if ($dir!="") {$dir.="/";}
		$destination=$dir.$parts[count($parts)-1];
		file_put_contents($destination,$source);
	}
	header('Location: index.php');
} else {
	$oldSourceFiles=trim(file_get_contents("files.dat"));
	foreach ($newSourceFiles as $file) {
		$source=getGitlabSource(SOURCE,$branch,$file);
		if ($source=="") {die("Source $file not found");}
		$parts=explode("/",$file);
		$dir="";
		if (count($parts)>1) {
			for ($i=0;$i<count($parts)-1;$i++){
				if ($dir!="") {$dir.="/";}
				$dir.=$parts[$i];
				if (!file_exists($dir)) {mkdir($dir);}
			}
		}
		if ($dir!="") {$dir.="/";}
		$destination=$dir.$parts[count($parts)-1];
		if (!file_exists($destination)) {
			file_put_contents($destination,$source);
		} else {
			$old=file_get_contents($destination);
			if ($source!=$old) {
				file_put_contents($destination,$source);
			}
		}
	}
	foreach ($oldSourceFiles as $oldFile) {
		$found=false;
		foreach ($newSourceFiles as $file) {
			if ($oldFile==$file) {$found=true;}
		}
		if (!$found) {unlink($oldFile);}
	}
	file_put_contents("files.dat",$newSourceIndex);
	header('Location: index.php');
}

function getGitlabSource($root,$branch,$file){

	if ($root) {$level="";} else {$level="source/";}

	$fileUrl="https://gitlab.com/c4319/website/-/raw/$branch/{$level}{$file}?ref_type=heads";
	$curl = curl_init($fileUrl);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($curl, CURLOPT_NOBODY, true);
	curl_setopt($curl, CURLOPT_HEADER, true);

	$response = curl_exec($curl);
	$httpStatus = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);
	
	if ($httpStatus == 200) {
		 return @file_get_contents($fileUrl);
	} else {
		return "";
	}
}
?>