# Website

The website repo contains the source files that make-up the website of communitycoins.org 

## Getting started

If you want to make changes or proposals create a new branch and test it on your own server or use https://branch.communitycoins.org/merge.php?branch=your-branch-name. 

The source-directory of this website project contains the files and directories that make up the website of communitycoins.org

The root of this repository contains an index (files.dat) that must contain all files involved. It also contains merge.php which synchronizes the host with gitlab.



